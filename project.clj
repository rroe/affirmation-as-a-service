(defproject ass "0.1.0-SNAPSHOT"
  :plugins [[lein-ring "0.8.12"]]
  :ring {:handler affirm-backend.core/handler}
  :description "Affirmation as a Software Service"
  :url "http://dynamicdev.net/"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.7.0"]
                 [org.clojure/java.jdbc "0.4.2"]
                 [mysql/mysql-connector-java "5.1.6"]
                 [org.clojure/data.json "0.2.6"]
                 [liberator "0.13"]
                 [compojure "1.3.4"]
                 [cheshire "5.5.0"]
                 [cc.artifice/clj-ml "0.6.0"]
                 [ring/ring-core "1.2.1"]])
