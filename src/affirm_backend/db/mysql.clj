(ns affirm-backend.db.mysql
  (:require [clojure.java.jdbc :as db]
            [affirm-backend.utils :as utils]))

;; https://github.com/clojure/java.jdbc

(def mysql-db {
               :subprotocol "mysql"
               :subname "//127.0.0.1:3306/project_indawo"
               :user "clojure"
               :password "RedBullsAreGoodToDrink!"})

(defn delete-where
  {
    :doc "Function used to delete records from the database"
    :author "Richard Roe"
    :args {
           :table "Keyword representing the table to run the query against"
           :comparator "Keyword representing the field to compare to"
           :value "Value to compare field values to"}}
  [table comparator value]
  (db/delete! mysql-db table [(str (utils/keyword-to-str comparator) " = ?") value]))

(defn query
  {
    :doc "Function used to run arbitrary queries in the database"
    :author "Richard Roe"
    :args {
           :strquery "A vector containing the database query"}}
  [strquery]
  (db/query mysql-db strquery))

(defn select-all
  {
    :doc "Function to select all records from a given table"
    :author "Richard Roe"
    :args {
           :table "Keyword representing a database table"}}
  [table]
  (query
    [(str "SELECT * FROM " (utils/keyword-to-str table) " WHERE id <> ?") 0]))

(defn select-friends
  {
    :doc "Function to select all friends for a given user"
    :author "Richard Roe"
    :args {
           :userId "Id of the user whose friends you wish to receive"}}
  [userId]
  (query
    ["SELECT friendId FROM connection WHERE user1Id = ?" userId]))

(defn select-on-id
  {
    :doc "Function to select a record from a given table"
    :author "Richard Roe"
    :args {
           :table "Keyword representing a database table"
           :id "The id of the record to be retrieved"}}


  [table id]
  (query
    [(str "SELECT * FROM " (utils/keyword-to-str table) " WHERE id = ?") id]))

(defn select-images-by-postId
  [postId]
  (query
    ["SELECT data FROM image WHERE postId = ?" postId]))

(defn get-like-count
  [postId]
  (query
    ["SELECT COUNT(*) FROM likes WHERE postId = ?" postId]))

(defn get-like-users
  [postId]
  (query
    ["SELECT userId FROM likes WHERE postId = ?" postId]))

(defn select-comments-on-postId
  [postId]
  (query
    ["SELECT * FROM response WHERE requestId = ?" postId]))

(defn select-on
  {
    :doc "Function to select a record from a given table"
    :author "Richard Roe"
    :args {
           :table "Keyword representing a database table"
           :field "Keyword of the field to compare to"
           :value "The comparator"}}


  [table field value]
  (query
    [(str "SELECT * FROM " (utils/keyword-to-str table) " WHERE ? = ?") (utils/keyword-to-str field) value]))

(defn select-on-toId
  {
    :doc "Function to select a record from a given table"
    :author "Richard Roe"
    :args {
           :toId "The toId of the pending record to be retrieved"}}


  [toId]
  (query
    [(str "SELECT * FROM pending WHERE toId = ?") toId]))

(defn select-on-email
  {
    :doc "Function to select a record from a given table"
    :author "Richard Roe"
    :args {
           :table "Keyword representing a database table"
           :email "The email of the record to be retrieved"}}


  [table email]
  (query
    [(str "SELECT * FROM " (utils/keyword-to-str table) " WHERE email = ?") email]))

(defn insert
  {
    :doc "Function to insert a record into a given table"
    :author "Richard Roe"
    :args {
           :table "Keyword representing a database table"
           :record "Map of fields + values to be created as a record"}}


  [table record]
  (db/insert! mysql-db table record))

(defn update-on-id
  {
    :doc "Function to update a record in a given table based on a given id"
    :author "Richard Roe"
    :args {
           :table "Keyword representing a database table"
           :record "Map of fields + values to be created as a record"
           :id "id of the record to update"}}


  [table record id]
  (db/update! mysql-db table record ["id = ?" id]))

(defn delete-on-id
  {
    :doc "Function to delete a record from a given table based on a given id"
    :author "Richard Roe"
    :args {
           :table "Keyword representing a database table"
           :id "The id of the record to be deleted"}}


  [table id]
  (db/delete! mysql-db table ["id = ?" id]))
