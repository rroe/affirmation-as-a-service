(ns affirm-backend.ml.model
  (:require [clojure.data.json :as json]
            [clojure.java.io :as io]
            [liberator.core :refer [resource defresource]]
            [ring.middleware.params :refer [wrap-params]]
            [compojure.core :refer [defroutes ANY]]
            [affirm-backend.utils :as utils]
            [cheshire.core :as cheshire]))

(use 'clj-ml.classifiers 'clj-ml.data 'clj-ml.utils 'clj-ml.io)

(defn get-via-ml []
  (let [dataset  (-> (load-instances :arff "file:///home/rroe/Documents/Clojure/Hackathon/affirmation-as-a-software-service/new-training-file.arff") (dataset-set-class :output))
       instance (-> (first (dataset-seq dataset)) (instance-set-class-missing))
       classifier (deserialize-from-file "/home/rroe/Documents/Clojure/Hackathon/affirmation-as-a-software-service/trained.bin")]
   (classifier-classify classifier (-> (make-instance dataset [(rand-int 1000) 0]) (instance-set-class-missing)))))
