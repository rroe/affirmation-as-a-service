(ns affirm-backend.res.routesources
  (:require [clojure.data.json :as json]
            [clojure.java.io :as io]
            [liberator.core :refer [resource defresource]]
            [ring.middleware.params :refer [wrap-params]]
            [compojure.core :refer [defroutes ANY]]
            [affirm-backend.utils :as utils]
            [cheshire.core :as cheshire]
            [affirm-backend.db.mysql :as sql]
            [affirm-backend.res.responses.handlers :as handlers]))

(defn get-from-query
  {
    :doc "Pulls information from the GET parameters associated with a given key"
    :author "Richard Roe"
    :args {
           :ctx "The Request context"
           :data "The key"}}
  [ctx data]
  (str (get-in ctx [:request :params data])))

(defn get-affirmation
  {
    :doc "Creates the resource for the /affirm route"
    :author "Richard Roe"}
  []
  (resource
    :allowed-methods [:get]
    :available-media-types ["application/json" "text/html"]
    :handle-ok handlers/affirmation-get))
