(ns affirm-backend.res.responses.handlers
  (:require [clojure.data.json :as json]
            [clojure.java.io :as io]
            [liberator.core :refer [resource defresource]]
            [ring.middleware.params :refer [wrap-params]]
            [compojure.core :refer [defroutes ANY]]
            [affirm-backend.utils :as utils]
            [cheshire.core :as cheshire]
            [affirm-backend.ml.model :as ml]
            [affirm-backend.db.mysql :as sql]))

(def data-adj ["amazing" "awesome" "fantastic" "wonderful" "fantastical" "great" "cool"])

(def data-mid ["super" "incredibly" "wonderfully" "fantastically" "extremely" "very"])

(defn get-from-query
  {
    :doc "Pulls information from the GET parameters associated with a given key"
    :author "Richard Roe"
    :args {
           :ctx "The Request context"
           :data "The key"}}
  [ctx data]
  (str (get-in ctx [:request :params data])))

(defn to-number [val]
  (Integer/parseInt (name val)))

(defn affirmation-get
  {
    :doc "Handles all get requests to /affirm"
    :author "Richard Roe"
    :args {:ctx "The request context"}}
  [ctx]
  (println (to-number (ml/get-via-ml)))
  (let [adj (get data-adj (mod (* (to-number (ml/get-via-ml)) (rand-int 10000)) (count data-adj)))
        mid (get data-mid (mod (* (to-number (ml/get-via-ml)) (rand-int 10000)) (count data-adj)))
        out {:message (str "You are " mid " " adj "!")}]
    out))
