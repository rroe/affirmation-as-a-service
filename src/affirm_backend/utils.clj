(ns affirm-backend.utils)

(defn str-tail
  {
    :doc "Retrieves the tail of a given string"
    :author "Richard Roe"
    :args {
           :data "The specified string"}}
  [data]
  (subs (str data) 1))

(defn keyword-to-str
  {
    :doc "Converts a given interned Keyword into a String representation"
    :author "Richard Roe"
    :args {
           :data "The specified Keyword"}}
  [data]
  (name data))

(defn take-from-to
  {
    :doc "Retrieves a certain number of elements between k and n"
    :author "Richard Roe"
    :args {
           :k "The number of pages to skip"
           :n "The number of elements to take"
           :data "The sequence to take from"}}
  [k n data]
  (take n (drop k data)))

(def not-nil? (complement nil?))

(defn haversine
  [{lon1 :longitude lat1 :latitude} {lon2 :longitude lat2 :latitude}]
  (let [R 6372.8
        dlat (Math/toRadians (- lat2 lat1))
        dlon (Math/toRadians (- lon2 lon1))
        lat1 (Math/toRadians lat1)
        lat2 (Math/toRadians lat2)
        a (+ (* (Math/sin (/ dlat 2)) (Math/sin (/ dlat 2))) (* (Math/sin (/ dlon 2)) (Math/sin (/ dlon 2)) (Math/cos lat1) (Math/cos lat2)))]
    (* R 2 (Math/asin (Math/sqrt a)))))

(defn distance
  [a1 b1 a2 b2]
  (* 0.621371 (haversine {:latitude a1 :longitude b1} {:latitude a2 :longitude b2})))

(defn within?
  [lat long dist other]
  (let [fin (distance lat long (:latitude other) (:longitude other))]
    (< fin dist)))
