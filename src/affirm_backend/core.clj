(ns affirm-backend.core
  (:require [clojure.data.json :as json]
            [clojure.java.io :as io]
            [liberator.core :refer [resource defresource]]
            [ring.middleware.params :refer [wrap-params]]
            [compojure.core :refer [defroutes ANY]]
            [affirm-backend.utils :as utils]
            [cheshire.core :as cheshire]
            [affirm-backend.db.mysql :as sql]
            ; TODO:
            ; [ring.middleware.cors :refer [wrap-cors]]
            [affirm-backend.res.routesources :as routes]))

;; http://clojure-liberator.github.io/liberator/

(defroutes app
  (ANY "/affirm" [] (routes/get-affirmation)))

; (def handler
;   (wrap-cors my-routes :access-control-allow-origin [#"http://example.com"]
;                        :access-control-allow-methods [:get :put :post :delete]))

(def handler
  (-> app
      wrap-params))
